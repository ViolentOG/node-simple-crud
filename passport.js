//var db = require('./db');
const User = require('./models/user');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id).then(function (user) {
        done(null, user);
    }).catch(function (err) {
        done(err);
    });
    /*db.one('SELECT * FROM "user" WHERE id = $1', [id]).then(function (user) {
        done(null, user);
    }).catch(function (err) {
        done(err);
    });*/
});

passport.use(new LocalStrategy(
    function(username, password, done) {
        User.findOne({where: {email: username, password_hash: password}}).then(function (user) {
            if(user === null) {
                done(null, false, {message: 'Incorrect login data.'});
                return;
            }
            done(null, user);
        }).catch(function (err) {
            done(err);
        });
        /*db.oneOrNone('SELECT id FROM "user" WHERE email = $1 AND password_hash = $2', [username, password]).then(function (data) {
            if(!data) {
                done(null, false, {message: 'Incorrect username.'});
                return;
            }
            done(null, data);
        }).catch(function (err) {
            done(err);
        });*/
    }
));

module.exports = passport;
const db = require('../db');
const Sequelize = require('Sequelize');

const User = db.define('user', {
    username: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    password_hash: {
        type: Sequelize.STRING
    }
}, {
    freezeTableName: true,
    tableName: 'user',
    createdAt: false,
    updatedAt: false
});

module.exports = User;
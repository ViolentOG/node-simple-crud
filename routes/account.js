var express = require('express');
var router = express.Router();
var passport = require('../passport');





router.get('/login', function(req, res, next) {

    res.render('account/login', { title: 'Login page' });
});

router.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login'
}), function (req, res, next) {

});

module.exports = router;

var express = require('express');
var pathToRegexp = require('path-to-regexp');
var router = express.Router();
const User = require('../models/user');


router.use(function (req, res, next) {
    if(req.user)
        next();
    else
        res.redirect('/login');
});

router.get('/', function(req, res, next) {

    User.findAll().then(function (users) {
        res.locals.users = users;
        next();
    }).catch(function (err) {
        next(err);
    })
}, function (req, res, next) {
    res.render('users/index');
});


router.get(pathToRegexp('/delete/:id'), function (req, res, next) {
    var id = req.params[0];
    db.result('DELETE FROM "user" WHERE id = $1', id).then(function () {
        res.redirect('/')
    });
});

router.get(pathToRegexp('/edit/:id?'), function (req, res, next) {

    var id = req.params[0];

    res.locals.model = {};
    if(id !== undefined) {
        User.findById(id).then(function (user) {
            res.locals.model = user;
            next();
        }).catch(function (err) {
            next(err);
        });
    } else {
        next();
    }
}, function (req, res, next) {
    res.render('users/edit');
});

router.post(pathToRegexp('/edit/:id?'), function (req, res, next) {
    var id = req.params[0];
    var model = new User();

    var cont = function () {
        if(req.body.username)
            model.username = req.body.username;
        if(req.body.email)
            model.email = req.body.email;
        if(req.body.password)
            model.password_hash = req.body.password;
        res.locals.model = model;
        next();
    };
    if(id !== undefined) {
        User.findById(id).then(function (user) {
            model = user;
            cont();
        }).catch(function (err) {
           next(err);
        });
    } else {
        cont();
    }

}, function (req, res, next) {
    var model = res.locals.model;

    model.save().then(function () {
        res.redirect('/users');
    }).catch(function (err) {
        next(err);
    });
});


module.exports = router;

/*var pgp = require('pg-promise')();
var conf = require('config.json')('./db-conf.json');

var c = pgp("postgres://" + conf.user + ":" + conf.password + "@" + conf.host + ":" + conf.port + "/" + conf.dbname);
module.exports = c;*/

var conf = require('config.json')('./db-conf.json');

var Sequelize = require('sequelize');

const sequelize = new Sequelize(conf.dbname, conf.user, conf.password, {
    host: conf.host,
    dialect: 'postgres',

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});

module.exports = sequelize;